import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);

  List<Widget> _crearItemsCorta1(){
    return options.map((item){
      
      return Column(
        children: <Widget>[
          ListTile(title: Text(item), subtitle: Text('Cualquier cosa')),
          Divider()
        ],
      );

    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),

      body: ListView(
        children: _crearItemsCorta1()
      ),

      /*
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/imagen1.png'),
            Icon(Icons.beach_access, color:Colors.blue, size: 36.0),
          ],
        ),
      ),
      */

      /*
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.adb, color: Colors.red, size: 50.0),
            Icon(Icons.beach_access, color: Colors.blue, size:36.0),
          ],
        ),
      ),
      */

      /*
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
          ],

        ),
      ),
      */

      /*
      body: Align(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Uno', style: styleText),
            Text('Dos', style: styleText),
            Text('Tres', style: styleText),
          ],
        ),
      ),
      */

    );
  }
}
