import 'package:flutter/material.dart';
import 'package:project02_lab12/src/pages/home_temp.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      home: HomePageTemp(),
      );
  }
}